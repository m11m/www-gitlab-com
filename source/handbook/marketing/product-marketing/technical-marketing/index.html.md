---
layout: markdown_page
title: "Technical Marketing"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Technical marketing at GitLab

![Golden Circle](/handbook/marketing/product-marketing/images/goldencircle.png)

#### Why (purpose)
Technical marketing exists to evangelize our products to both technical and non technical audiences by expressing the technical capabilities of the product as values to our target audiences.

#### How (process)
We do this by teaching our audiences about modern software delivery methods and how it can be valuable to them, introducing them to new concepts which can help them achieve their goals, and by showcasing the capabilities of the product for the use cases which our audiences care about.

#### What (output)
We produce demos, videos, workshops, tutorials, technical white papers, conference presentations, webinars, and customer engagements such as with sales, at conference booths, and community and customer meetups.

View the [Technical Marketing Issue Board](https://gitlab.com/gitlab-com/marketing/product-marketing/-/boards/926375?&label_name[]=tech-pmm) to see what the TMM team is working on.

##### Demos

Demos help show the value GitLab can bring to customers. Go to the [Demo page](/handbook/marketing/product-marketing/demo/) to see what's available and get more info.

### Which technical marketing manager should I contact?

- Listed below are areas of responsibility within the product marketing team:

  - [Tye](/company/team/#TyeD19), TMM (Dev)
  - [Itzik](/company/team/#itzikgb), TMM (Verify)
  - [Cullen](/company/team/#eggshellcullen), TMM, Cloud Native
  - [Dan](/company/team/#dbgordon), Manager TMM (CI/CD & Ops)
  - [Ashish](/company/team/#kuthiala), Director PMM
