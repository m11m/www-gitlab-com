---
layout: markdown_page
title: "Book clubs"
---

## On this page
{:.no_toc}

- TOC
{:toc}

From time to time, we run internal book clubs on a [book from our
leadership page](/handbook/leadership/#books). To propose a new book
club, [create an
issue](https://gitlab.com/gitlab-com/book-clubs/issues/new) in the [book
clubs project](https://gitlab.com/gitlab-com/book-clubs).

## High Output Management

* Dates: 2019-06-03 to 2019-07-15 (every two weeks)
* Time: 7:30 Pacific Time (one hour before the company call)
* [Zoom](https://gitlab.zoom.us/j/544984602)
* [Meeting agenda](https://docs.google.com/document/d/1gQZahLk2LYDbYAb4TeYqNOQbF8f6MrCaATLyPTzRONY/edit)
* [Discussion issue](https://gitlab.com/gitlab-com/book-clubs/issues/3)

Recorded parts of meetings are available in the [YouTube playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kp_xR7vrRiGu7URKiUmN8m3):

<iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?list=PL05JrBw4t0Kp_xR7vrRiGu7URKiUmN8m3" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

## Crucial Conversations

This book club was internal-only, intended specifically for engineering
managers.

* Dates: 2018-10-01 to 2018-11-05
* [Notes](https://docs.google.com/document/d/1lY-v9zRdSxtVKu-yh3U7oz5kNen2YZE_m5OeNF0QNHM/edit)
* [Recordings](https://drive.google.com/drive/u/0/folders/1lqtdN4eWLG0RxqV8KSsnp8__P__Bff-2)

## Suggestions on running a book club

1. If possible, find a partner. Having two people run the book club has
   a number of advantages:
    1. Reduced pressure and workload.
    2. Lower chance of needing to reschedule to to illness or other
       emergency.
    3. Meetings start as a conversation, not a monologue.
2. Add the book's title to the handbook if it isn't already listed.
    1. Books that are also available as audiobooks increase the
       potential audience. The more ways people can experience the book,
       the more people can attend.
3. Set up the meeting schedule.
    1. Record the meetings and [post them to
       YouTube](/handbook/communication/youtube/). If the participants
       agree, make the videos public.
    2. Consider having the same section of the book covered in different
       meeting slots, to allow people in different time zones to attend.
4. Set the expected reading schedule for the entire book before
   starting, so people know what level of reading is required.
5. Create an agenda for each session seeded with interesting quotes and
   concepts from the relevant section of the book.
     1. If possible, prepare topics for all of the sessions before the starting,
        too - although this requires the hosts to read the whole book up front.
6. Once the book club is done, seek feedback and update this handbook
   page!
