---
layout: markdown_page
title: "Category Vision - Release Governance"
---

- TOC
{:toc}

## Release Governance

Release Governance is all about the controls and automation (security,
compliance, or otherwise) that ensure your releases are managed in an
auditable and trackable way, in order to meet the need of the business
to understand what is changing.

This includes features like having a strong integration with CI/CD to
ensure an auditable chain of custody for artifacts and traceability all the way back
to the commits and issues that made up the release, confirming requirements
such as test completion or other quality and security gates, appropriate
approvals are gathered in the process, and so on. Our intention is to achieve this as a natural byproduct of your way of working
within GitLab and not requiring any additional effort or attention needed in order to prepare for compliance and audit tracking.


Related to this topic, but complex enough to be its own category, is
[secrets management](/direction/release/secrets_management).

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ARelease%20Governance)
- [Overall Vision](/direction/release)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/594)

Interested in joining the conversation for this category? Please join us in our
[public epic](https://gitlab.com/groups/gitlab-org/-/epics/1297) where
we discuss this topic and can answer any questions you may have. Your contributions
are more than welcome.

## What's Next & Why

Up next is evidence collection for SDLC [gitlab-ce#56030](https://gitlab.com/gitlab-org/gitlab-ce/issues/56030). It is important to know everything
that happens in your software delivery pipeline. We want to make it easy to answer: what, where, when, and by whom any change in the software happened,
without any additional effort, by making this data accessible from the release itself. 


## Maturity Plan

This category is currently at the "Planned" maturity level, and
our next maturity target is Viable (see our [definitions of maturity levels](/direction/maturity/)).
Key deliverables to achieve this are:

- [Wait for approvals in pipelines](https://gitlab.com/gitlab-org/gitlab-ee/issues/9187)
- [Evidence collection MVC](https://gitlab.com/gitlab-org/gitlab-ce/issues/56030)

## Competitive Landscape

A key capability of products which securely manage releases is to collect
evidence associated with releases in a secure way. [gitlab-ce#56030](https://gitlab.com/gitlab-org/gitlab-ce/issues/56030)
introduces a new kind of entity that is part of a release, which contains
various kinds of evidence (test results, security scans, etc.) that
were collected as part of a release generation. Collecting this data and
surfacing it in a clear way for auditors is a great differentiator for GitLab,
and one that is uniquely enabled for us by being a single application for the
DevOps lifecycle.

## Analyst Landscape

The analysts in this space tend to focus a lot right now on existing, more
legacy style deployment workflows so changes like [gitlab-ee#9187](https://gitlab.com/gitlab-org/gitlab-ee/issues/9187)
(better support for validation of approvals in the pipeline) will help
us perform better here, as well as for the kinds of customers who are
really need a bit more control over their delivery process.

Similarly, integrations with technologies like ServiceNow ([gitlab-ee#8373](https://gitlab.com/gitlab-org/gitlab-ee/issues/8373))
will help GitLab fit in better with larger, pre-existing enterprise governance workflows.

## Top Customer Success/Sales Issue(s)

The CS team frequently sees requests for integration with ServiceNow for change
management built in to CD pipelines, as per [gitlab-ee#8373](https://gitlab.com/gitlab-org/gitlab-ee/issues/8373).

## Top Customer Issue(s)

[gitlab-ee#9187](https://gitlab.com/gitlab-org/gitlab-ee/issues/9187) is
the most upvoted item and adds a way to validate approvals
in release workflows.

## Top Internal Customer Issue(s)

[gitlab-ce#21583](https://gitlab.com/gitlab-org/gitlab-ce/issues/21583), which
implements user access controls for environments, has been requested by our own
delivery team to allow for more secure, locked down access to production-type
environments instead of relying on more broad-based project permissions.

### Features of Interest

 - [Releases Page](https://gitlab.com/gitlab-org/gitlab-ce/releases)
 - [gitlab-ee#9187](https://gitlab.com/gitlab-org/gitlab-ee/issues/9187) (Approval jobs)
 - [gitlab-ce#56030](https://gitlab.com/gitlab-org/gitlab-ce/issues/56030) (Evidence collection)

### Ongoing efforts to support

 - [Internal change management process](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/231)
 - [Service lifecycle workflow](https://about.gitlab.com/handbook/engineering/security/guidance/SLC.1.01_service_lifecycle_workflow.html)
 - [Controls by family](https://about.gitlab.com/handbook/engineering/security/sec-controls.html#list-of-controls-by-family)

## Top Vision Item(s)

Important for this category (though also expansive and includes a few others) is
our epic for [locking down the path to production](https://gitlab.com/groups/gitlab-org/-/epics/762),
which will help us successfully deliver compliance controls within the software delivery pipeline.

Also related to locking down the path to production is binary
authorization ([gitlab-ee#7268](https://gitlab.com/gitlab-org/gitlab-ee/issues/7268))
which provides a secure means to validate deployable containers. At the
moment however this only works with GKE so ultimate user adoption is limited. As such we're
keeping an eye on adoption, but have not yet implemented an MVC.

Blackout periods ([gitlab-ce#51738](https://gitlab.com/gitlab-org/gitlab-ce/issues/51738))
will help compliance teams enforce periods where production needs to remain stable/not change.
